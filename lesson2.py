# -*- coding: utf-8 -*-
# print('Hello World')

#=============================================
# Соблюдение РЕР8. Неправильние отступи
# print('Hello World')
# if 1==1:
# print('Hello World')

# =============================================
# Зарезервированние слова. Сдежебние слова!
# ints = 22
# print('ints: ', ints)
# string_number = '321'
# print(int(string_number))
# Исправление
# my_int = 200
# print('my_int: ', my_int)
# string_number = '654'
# nsrt = int(string_number)
# print('nsrt: ', nsrt)
# print('type(nsrt): ', type(nsrt))

# # =============================================
# # Определение типа переменной. Функция type()
# print(type(my_int))

# # =============================================
# Определение типов (int, float, str)
# my_int = 1522
# print('my_int: ', my_int)
# print('type(my_int): ', type(my_int))
# my_float = 122.4
# print('my_float: ', my_float)
# print('type(my_float): ', type(my_float))
# my_str = '15'
# print('my_str: ', my_str)
# print('type(my_str): ', type(my_str))

# =============================================
# Введение значений переменнх с клавиатури
# my_value = input('Введите любое значение:')
# print('my_value: ', my_value)
# print('type(my_value): ', type(my_value))
# # функция input всегда возвращает строку
# my_value = my_value*9
# print('my_value: ', my_value)
# print('type(my_value): ', type(my_value))

# # =============================================
# # Приведени======================================
# Введение значений переменнх с клавиатури
# my_value = input('Введите любое значение:')
# print('my_value: ', my_value)
# print('type(my_value): ', type(my_value))
# # # # функция input всегда возвращает строку
# my_value = my_value*9
# print('my_value: ', my_value)
# print('type(my_value): ', type(my_value))

# # =============================================
# # Приведение типа переменной
# my_value = int(my_value)
# print('my_value: ', my_value)
# print('my_value: ', my_value*2)
# print('type(my_value): ', type(my_value))

# my_str = '30'
# print('my_str: ', my_str)
# print('type(my_str): ', type(my_str))
# my_int = str(my_str)
# print('my_int: ', my_int)
# print('type(my_int): ', type(my_int))

# my_str = '55.2'
# print('float(my_str): ', float(my_str))
# print('int(my_str): ', int(my_str))

# my_int = 10.2
# my_float = float(my_int)
# print('my_float: ', my_float)
# print(my_float)

# my_float = 10.54
# print('int(my_float): ', int(my_float))

# my_float = 40.32
# print('int(my_float): ', int(my_float))
# my_float = -20.32
# print('int(my_float): ', int(my_float))
# # приведение типа не математическое округление, а просто отбрасивание десятичной сторони

# my_float = 63.75
# my_str = str(my_float)
# print('my_str: ', my_str)
# print('type(my_str): ', type(my_str))

# my_int = 10
# my_str = str(my_int)
# print('my_str: ', my_str)
# print('type(my_str): ', type(my_str))

# my_str = 'six'
# print('int(my_str: ', int(my_str))
# my_float = 234
# print('my_float: ', my_float)
# print('type(my_float): ', type(my_float))

# =============================================
# # Булевий тип данних
# my_bool = False
# print('my_bool: ', my_bool)
# print('type(my_bool): ', type(my_bool))

# my_bool = 4==3
# print('my_bool: ', my_bool)
# print('type(my_bool): ', type(my_bool))

#my_bool = 5!=6
# print('my_bool: ', my_bool)
# print('type(my_bool): ', type(my_bool))

# my_bool = 55==54
# print('my_bool: ', my_bool)
# print('type(my_bool): ', type(my_bool))

# my_bool = 2!=3
# print('my_bool: ', my_bool)
# print('type(my_bool): ', type(my_bool))

# my_bool = 2>2
# print('my_bool: ', my_bool)
# print('type(my_bool): ', type(my_bool))

# my_bool = 2>=2
# print('my_bool: ', my_bool)
# print('type(my_bool): ', type(my_bool))

# my_bool = 21>=2
# print('my_bool: ', my_bool)
# print('type(my_bool): ', type(my_bool))

# my_bool = 33>=5
# print('my_bool: ', my_bool)
# new_bool = not my_bool
# print('new_bool: ', new_bool)
# print('type(new_bool): ', type(new_bool))

# my_bool = 23>=46
# print('my_bool: ', my_bool)
# new_bool = not my_bool
# print('new_bool: ', new_bool)
# print('type(new_bool): ', type(new_bool))

# my_int = 0
# print('bool(my_int): ', bool(my_int))
# print('type(my_int): ', type(my_int))

# my_int = -1
# print('bool(my_int): ', bool(my_int))
# print('type(my_int): ', type(my_int))

# my_int = 1
# print('bool(my_int): ', bool(my_int))
# print('type(my_int): ', type(my_int))


# my_int = -1.7
# print('bool(my_int): ', bool(my_int))
# print('type(my_int): ', type(my_int))

# my_int = 0.0
# print('bool(my_int): ', bool(my_int))
# print('type(my_int): ', type(my_int))

# Зміна точності
# my_int = 0.222222222222222222222222222221111
# print('bool(my_int): ', bool(my_int))
# print('type(my_int): ', type(my_int))

# val_1 = 22.334634576
# val_2 = 22.334634575
# print(val_1==val_2)
# print(abs(val_1 - val_2) < 0.001)

# my_str = '0'
# print('bool(my_str): ', bool(my_str))

# my_str = '1'
# print('bool(my_str): ', bool(my_str))

# my_str = ' '
# print('bool(my_str): ', bool(my_str))

# my_str = 'False'
# print('bool(my_str): ', bool(my_str))

# my_bool = False
# print('my_bool: ', my_bool)
# my_int = int(my_bool)
# print('my_int: ', my_int)
# print('type(my_int): ', type(my_int))
# my_str = str(my_bool)
# print('my_str: ', my_str)
# print('type(my_str): ', type(my_str))

# my_str = 'False'
# print('bool(my_str): ', bool(my_str))

# my_bool = True
# print('my_bool: ', my_bool)
# my_int = int(my_bool)
# print('my_int: ', my_int)
# print('type(my_int): ', type(my_int))
# my_str = str(my_bool)
# print('my_str: ', my_str)
# print('type(my_str): ', type(my_str))
# print('bool(my_str): ', bool(my_str))
# print('type(my_str): ', type(my_str))

# my_val = None
# print('bool(my_val): ', bool(my_val))

# my_val = NotImplemented
# print('bool(my_val): ', bool(my_val))

# my_val = Ellipsis
# print('bool(my_val): ', bool(my_val))

# ================================================
# Условний оператор if
# current_temperature = int(input('Сколько сейчас градусов на улице:'))
# if current_temperature > 5:
#     print('Одень шапку!')
# else:
#     print('Можеш не одевать шапку')

# current_temperature = int(input('Сколько сейчас градусов на улице:'))
# if current_temperature < 6:
#     print('Одень шапку!')
# elif current_temperature > 20:
#     print('Одень кепку!')
# else:
#     print('Можеш не одевать шапку')

# current_temperature = int(input('Сколько сейчас градусов на улице:'))
# if current_temperature < 5:
#     print('Одень шапку!')
# elif current_temperature > 25:
#     print('Одень кепку!')
# elif current_temperature < -25: # ошибка последовательности условного оператора
#     print('Останься дома!')
# else:
#     print('Можеш не одевать шапку')

# my_str = ''
# if my_str:
#     print(my_str)