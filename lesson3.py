# print('Hello World')

# d = {'wdwdwd'}
# print(dir(d)) 

# capitalize()	Converts the first character to upper case
# strg = 'heLLO WoRlD!'
# print(strg.capitalize())

# # casefold()	Converts str into lower case
# strg = 'HeLLO WoRlD!'
# print(strg.casefold())

# # center()	Returns a centered str
# strg = 'HeLLO and WoRlD!'
# st = strg.center(1)
# print(st)

# count()	Returns the number of times a specified value occurs in a str
# strg = 'HeLLO and WORlD up down'
# st = strg.count("up")
# print(st)

# encode()	Returns an encoded version of the str
# strg = 'Привет, мир!'
# st = strg.encode()
# print(st)

# endswith()	Returns true if the str ends with the specified value
# strg = 'Привет, мир!'
# st = strg.endswith('!')
# print(st) 

# # expandtabs()	Sets the tab size of the str
# strg = "111 \tпример \tстроки"
# print ("Original string: " + str)
# print ("Additional string " +  strg.expandtabs())
# print ("Tabulating " +  strg.expandtabs(10))

# find()	Searches the str for a specified value and returns the position of where it was found
# str1 = "Привет, облачный мир"
# str2 = "облачный";
# print (str1.find(str2))
# print (str1.find(str2, 4))
# print (str1.find(str2, 10))

# format()	Formats specified values in a str
# quantity = 4
# itemno = 357
# price = 72.43
# myorder = "I want {} pieces of item {} for {} dollars."
# print(myorder.format(quantity, itemno, price))
# myorder = f"I want to pay {price} dollars for {quantity} pieces of item {itemno}."
# print(myorder)
# myorder = "I want %s pieces of item %s for %s dollars." % (quantity, itemno, price)
# print(myorder)

# format_map()	Formats specified values in a str
# a={'x':'Eduard', 'y':'Morganssss'} 
# print("{x}' last name is {y}".format_map(a)) 

# index()	Searches the str for a specified value and returns the position of where it was found
# vowels = ['a', 'e', 'i', 'o', 'i', 'u']
# index = vowels.index('e')
# print('The index of e:', index)
# index = vowels.index('i')
# print('The index of i:', index)

# isalnum()	Returns True if all characters in the str are alphanumeric
# p='abc'.isalnum()
# print(p)

# isalpha()	Returns True if all characters in the str are in the alphabet
# s='qfqw123fDFDFDF'.isalpha()
# print(s)

# isdecimal()	Returns True if all characters in the str are decimals
# p='123123'.isdecimal()
# print(p)

# isdigit()	Returns True if all characters in the str are digits
# p='213231'.isdigit()
# print(p)

# isidentifier()	Returns True if the str is an identifier
# p='con'.isidentifier()
# print(p)

# islower()	Returns True if all characters in the str are lower case
# p='нижний loweR'.islower()
# print(p)

# isnumeric()	Returns True if all characters in the str are numeric
# p='12'.isnumeric()
# print(p)

# isprintable()	Returns True if all characters in the str are printable
# p=''.isprintable()
# print(p)

# isspace()	Returns True if all characters in the str are whitespaces
# p='!@#'.isspace()
# print(p)

# istitle() 	Returns True if the str follows the rules of a title
# p='Hello'.istitle()
# s='hello'.istitle()
# print(s,p)

# isupper()	Returns True if all characters in the str are upper case
# txt = "HELLO WORLD"
# x = txt.isupper()
# print(x)

# join()	Joins the elements of an iterable to the end of the str
# list= ("Eduard", "Pavel", "Guru")
# x = "@".join(list)
# print(x) 

# ljust()	Returns a left justified version of the str
# txt = "hello"
# x = txt.ljust(30)
# print(x, "BIG world") 

# lower()	Converts a str into lower case
# txt = "HELLO big WORld"
# x = txt.lower()
# print(x) 

# lstrip()	Returns a left trim version of the str
# txt = "     big     "
# x = txt.lstrip()
# print("Hello", x, "world and peaple") 

# maketrans()	Returns a translation table to be used in translations
# txt = "hello Sam"
# mytable = txt.maketrans("S", "X")
# print(txt.translate(mytable))

# partition()	Returns a tuple where the str is parted into three parts
# txt = "hello big world and peaple"
# x = txt.partition("big")
# print(x) 

# replace()	Returns a str where a specified value is replaced with a specified value
# txt = "hello world"
# x = txt.replace("world", "мир")
# print(x) 

# rfind()	Searches the str for a specified value and returns the last position of where it was found
# txt = "hello world"
# x = txt.rfind("w")
# print(x)

# rindex()	Searches the str for a specified value and returns the last position of where it was found
# txt = "hello world"
# x = txt.rindex("w")
# print(x) 

# rjust()	Returns a right justified version of the str
# txt = "Hello"
# x = txt.rjust(10)
# print(x, "Worlds BIG")

# rpartition()	Returns a tuple where the str is parted into three parts
# txt = "Hello wiffwf BIG"
# x = txt.rpartition("BIG")
# print(x) 

# rsplit()	Splits the str at the specified separator, and returns a list
# txt = "big, small, checks"
# x = txt.rsplit(", ")
# print(x)

# rstrip()	Returns a right trim version of the str
# p='Hello wolds'.rstrip('ds')
# print(p)

# split()	Splits the str at the specified separator, and returns a list
# p='1,2,3,5,8,10'.split(',', maxsplit=4)
# print(p)

# splitlines()	Splits the str at line breaks and returns a list
# txt = "Hello \nWelcome"
# x = txt.splitlines()
# print(x) 

# startswith()	Returns true if the str starts with the specified value
# txt = "hello, world"
# x = txt.startswith("Hello")
# print(x) 

# strip()	Returns a trimmed version of the str
# txt = "                                 banana     "
# x = txt.strip()
# print("of all ruits", x, "is my favorite") 

# swapcase()	Swaps cases, lower case becomes upper case and vice versa
# p='Приветствую мира!'.swapcase()
# print(p)

# title()	Converts the first character of each word to upper case
# p='привет мир!'.title()
# print(p)

# translate()	Returns a translated str
# trans_table = strg.maketrans({'о': 's', 'л': 'к', '—': None})
# replaced = 'кот — бsл'.translate(trans_table)

# upper()	Converts a  into upper case
# ps='HeLoW WoRld'
# after=ps.upper()
# print("до -",ps)
# print("после -",after)

# zfill()	Fills the str with a specified number of 0 values at the beginning
# p='bb1'.zfill(10)
# print(p)